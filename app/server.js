var express = require('express');
var app = express();

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var bodyParser = require('body-parser');

var User = require("./models/user.js");


const util = require('util')


app.use(bodyParser());

passport.use("local-signup", new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true,
        session: false
    },
    function (req, username, password, done) {
        console.log("register ongoing for user: " + username);
        User.Schema.findOne({ where: {username: username} }).then(user => {
            if (!user) {
                console.log("success, can register.");
                return done();
            }
            return done({code:"?", message: 'User already existing.' });
        });
  }
));

passport.use("local-signin", new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true,
        session: false
    },
    function (req, username, password, done) {
        console.log("authentication ongoing for user: " + username);
        User.Schema.findOne({ where: {username: username} }).then(user => {
            if (user && User.Helper.isValidPassword(user, password)) {
                done(null, user);
            }
            else {
                return done({code:500, message: 'Access denied.' });
            }
        });
  }
));

app.use(passport.initialize());

app.post('/signup', function(req, res) {
    passport.authenticate('local-signup', function (err, user, info) {
        if (err) {
            return res.send(err);
        }
        console.log("test");
        user = User.Schema.build({username: req.body.username});
        User.Helper.updatePassword(user, req.body.password);
        res.send('got a put request to signup');
        console.log(user.passwordHash);
})(req, res)});

app.post('/signin', function(req, res) {
    passport.authenticate('local-signin', function (err, user, info) {
        if (err) {
            return res.send(err);
        }
        if (!user) {
            return res.send(info);
        }
        console.log("user: " + user);
        res.send('authentication passed.');
})(req, res)});

app.use(function(req, res, next) {
  res.status(404).send('Sorry cant find that!');
});

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});