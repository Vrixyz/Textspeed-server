const Sequelize = require('sequelize');
const sequelize = new Sequelize('textspeed_db', '-', '-');
var bcrypt = require('bcrypt');

var Schema = sequelize.define('user', {
    username: Sequelize.STRING,
    passwordHash: Sequelize.STRING,
    passwordSalt: Sequelize.STRING
}, {
    freezeTableName: true
});

function Helper() {
};

// private

var computeHash = function (password, salt) {
    return bcrypt.hashSync(password, salt);
};

//public

Helper.prototype.updatePassword = function(user, password) {
    user.passwordSalt = bcrypt.genSaltSync(8);
    user.passwordHash = computeHash(password, user.passwordSalt);
    return user.save();
};
Helper.prototype.isValidPassword = function (user, password) {
    if (!password ||password.length <= 0) {
        return false;
    }
    var passwordHashParameter = computeHash(password, user.passwordSalt.toString());
    return passwordHashParameter == user.passwordHash.toString().substring(0, passwordHashParameter.length);
};

module.exports.Schema = Schema;
module.exports.Helper = new Helper();