create table user (
	id int auto_increment not null,
    username varchar(64) not null,
    passwordHash binary(64) not null,
    passwordSalt binary(64) not null,
    createdAt DATE not null,
    updatedAt DATE not null,
    primary key (id)
);

drop table user;